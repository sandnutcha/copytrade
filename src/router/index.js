import Vue from 'vue'
import VueRouter from 'vue-router'
import Main from '../views/Main.vue'

//Dashboard
import Dashboard from '../views/Dashboard.vue'

//Slaves
import AllSlaves from '../views/Slaves/AllSlaves.vue'
import CreateSlaves from '../views/Slaves/CreateSlaves.vue'
import ShowAllSlaves from '../views/Slaves/ShowAllSlaves.vue'
import EditSlaves from '../views/Slaves/EditSlaves.vue'
//Edit Slaves
import AccountSettingSlaves from '../views/Slaves/EditSlaves/AccountSetting.vue'
import HistoryOrderSlaves from '../views/Slaves/EditSlaves/HistoryOrder.vue'

//Masters
import AllMasters from '../views/Masters/AllMasters.vue'
import CreateMaster from '../views/Masters/CreateMaster.vue'
import ShowAllMasters from '../views/Masters/ShowAllMasters.vue'
import EditMasters from '../views/Masters/EditMasters.vue'
//Edit Masters
import MySlaveMaster from '../views/Masters/EditMasters/MySlave.vue'
import HistoryOrderMaster from '../views/Masters/EditMasters/HistoryOrder.vue'
import AccountSettingMaster from '../views/Masters/EditMasters/AccountSetting.vue'

//Groups
import AllGroups from '../views/Groups/AllGroups.vue'
import CreateGroups from '../views/Groups/CreateGroups.vue'
//Edit Groups
import EditGroups from '../views/Groups/EditGroups.vue'
import GroupSettings from '../views/Groups/EditGroups/GroupSettings.vue'
import HistoryOrderGroup from '../views/Groups/EditGroups/HistoryOrder.vue'

//Presets
import AllPreset from '../views/Presets/AllPreset.vue'
import CreatePresets from '../views/Presets/CreatePresets.vue'
//Edit Presets
import EditPresets from '../views/Presets/EditPresets.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Main',
    redirect: '/dashboard',
    component: Main,
    children: [
      //Dashboard
      {
        path: '/dashboard',
        name: 'Dashboard',
        component: Dashboard
      },

      //Slaves
      {
        path: '/slaves',
        name: 'AllSlaves',
        component: AllSlaves
      }, {
        path: '/slaves/create',
        name: 'CreateSlaves',
        component: CreateSlaves
      }, {
        path: '/slaves/edit/:id',
        name: 'EditSlaves',
        redirect: { name: 'AccountSettingSlaves' },
        component: EditSlaves,
        children: [
          {
            path: 'AccountSetting',
            name: 'AccountSettingSlaves',
            component: AccountSettingSlaves
          },
          {
            path: 'HistoryOrder',
            name: 'HistoryOrderSlaves',
            component: HistoryOrderSlaves
          },]
      }, {
        path: '/slaves/showallslaves',
        name: 'ShowAllSlaves',
        component: ShowAllSlaves
      },

      //Master
      {
        path: '/masters',
        name: 'AllMasters',
        component: AllMasters
      },
      {
        path: '/masters/create',
        name: 'CreateMaster',
        component: CreateMaster
      }, {
        path: '/masters/showallmasters',
        name: 'ShowAllMasters',
        component: ShowAllMasters
      },
      {
        path: '/masters/edit/:id',
        name: 'EditMasters',
        redirect: { name: 'AccountSettingMaster' },
        component: EditMasters,
        children: [
          {
            path: 'AccountSetting',
            name: 'AccountSettingMaster',
            component: AccountSettingMaster
          },
          {
            path: 'MySlave',
            name: 'MySlaveMaster',
            component: MySlaveMaster
          },
          {
            path: 'HistoryOrder',
            name: 'HistoryOrderMaster',
            component: HistoryOrderMaster
          },]
      },

      //Groups
      {
        path: '/groups',
        name: 'AllGroups',
        component: AllGroups
      }, {
        path: '/groups/create',
        name: 'CreateGroups',
        component: CreateGroups
      }, {
        path: '/groups/edit/:id',
        name: 'EditGroups',
        redirect: { name: 'GroupSettings' },
        component: EditGroups,
        children: [
          {
            path: 'GroupSetting',
            name: 'GroupSettings',
            component: GroupSettings
          },
          {
            path: 'HistoryOrder',
            name: 'HistoryOrderGroup',
            component: HistoryOrderGroup
          },]
      },


      //Preset
      {
        path: '/presets',
        name: 'AllPreset',
        component: AllPreset
      }, {
        path: '/presets/create',
        name: 'CreatePresets',
        component: CreatePresets
      }, {
        path: '/presets/edit/:id',
        name: 'EditPresets',
        component: EditPresets,

      },
    ]
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
