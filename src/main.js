import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'

import axios from 'axios'
import VueAxios from 'vue-axios'
import Notifications from 'vue-notification'

Vue.use(Notifications)
Vue.use(VueAxios, axios)
Vue.config.productionTip = false
// Vue.prototype.$myIp = '127.0.0.1:5000'

//Rules
Vue.prototype.$rules_6min = [(v) => v.length >= 6 || "Min 6 characters"]
Vue.prototype.$rules_name = [
  v => !!v || 'Name is required',
  v => (v && v.length <= 10) || 'Name must be less than 10 characters',
]

Vue.prototype.$rules_require = [val => (val || '').length > 0 || 'This field is required']


new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')


