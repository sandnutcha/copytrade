import { HorizontalBar } from 'vue-chartjs'

export default {
    extends: HorizontalBar,
    props: ['data', 'label'],
    data: () => ({
        options: {
            legend: {
                display: false
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                    position: 'top',
                    ticks: {
                        // Include a dollar sign in the ticks
                        callback: function (value) {
                            if (value > 999) {
                                return value / 1000 + 'K'
                            }
                            return value;
                        }
                    }
                }],
                yAxes: [{
                    gridLines: {
                        display: false
                    },

                }],
                y: {

                }
            },
            tooltips: {
                callbacks: {
                    // title: function (item, data) {
                    //     return 'a'
                    // },
                    label: function (item) {
                        // var datasetLabel = data.datasets[item.datasetIndex].label || "";
                        var dataPoint = item.xLabel;
                        return "Balance (USD) : " + dataPoint / 1000 + "K";
                    }
                },

            },
        }
    }),
    mounted() {
        // this.chartData is created in the mixin.
        // If you want to pass options please create a local options object
        this.renderChart({
            labels: this.label,
            datasets: [
                {
                    label: "Data One",
                    backgroundColor: this.generateRandomColor(),
                    data: this.data,
                    barThickness: 20,
                    // categoryPercentage: 0.1,
                    // barPercentage: 0.1
                }
            ]
        }, this.options)


    },
    methods: {
        generateRandomColor() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            var color_set = [];
            for (var a = 0; a < this.label.length; a++) {
                color = '#'
                for (var i = 0; i < 6; i++) {
                    color += letters[Math.floor(Math.random() * 16)];
                }
                color_set.push(color);
                console.log(color_set);
            }
            return color_set;
        }
    },
}